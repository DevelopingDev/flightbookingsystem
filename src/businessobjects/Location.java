package businessobjects;

/**
 *
 * @author John Wang
 * @studentID 92016019
 *
 */
public class Location {
    
    private String airportCode;

    public String getAirportCode() {
        return airportCode;
    }

    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    private String city;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString(){
        return this.getCity() + " " + this.getAirportCode();
    }
}
