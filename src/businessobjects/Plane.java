package businessobjects;

/**
 *
 * @author John Wang
 * @studentID 92016019
 *
 */
public enum Plane {
    AIRBUSA350(270), AIRBUSA280(500), BOEING737(215), BOEING747(460);
   
    private int passengerCapacity;

    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    private Plane(int passengerCapacity) {
        this.passengerCapacity = passengerCapacity;
    }
    
    
 
}
