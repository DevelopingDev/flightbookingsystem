package businessobjects;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author John Wang
 * @studentID 92016019
 *
 */
public class Flight {
    
    SimpleDateFormat sdf = new SimpleDateFormat ("dd/MM/yyyy HH:mm:ss");
   
    
    private String flightNumber;
    
    private String DepartureAirport;

    private String destinationAirport;
    
    private double price;
    
    private Date DateTime;

    private Plane plane;

    private int seatsTaken;

    public int getSeatsTaken() {
        return seatsTaken;
    }

    public void setSeatsTaken(int seatsTaken) {
        this.seatsTaken = seatsTaken;
    }

    public String getPlane(){
        
        switch (this.plane) {
            case AIRBUSA350:
                return "AIRBUSA350";
            case AIRBUSA280:
                return "AIRBUSA280";
            case BOEING737:
                return "BOEING737";
            case BOEING747:
                return "BOEING747";
            default:
                throw new Error();
        }
    }

    public void setPlane(String plane) {
        // Will set appropreiate plane according to the string from parameter
        switch (plane) {
            case "AIRBUSA350":
                this.plane = Plane.AIRBUSA350;
                break;
            case "AIRBUSA280":
                this.plane = Plane.AIRBUSA280;
                break;    
            case "BOEING737":
                this.plane = Plane.BOEING737;
                break; 
            case "BOEING747":
                this.plane = Plane.BOEING747;
                break; 
            default:
                throw new AssertionError();
        }
    }    

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
    public Date getDateTime() {
        return DateTime;
    }

    public void setDateTime(Date DateTime) {
        this.DateTime = DateTime;
    }

    public String getDestinationAirport() {
        return destinationAirport;
    }

    public void setDestinationAirport(String destinationAirport) {
        this.destinationAirport = destinationAirport;
    }
    
    
    public String getDepartureAirport() {
        return DepartureAirport;
    }

    public void setDepartureAirport(String DepartureAirport) {
        this.DepartureAirport = DepartureAirport;
    }
    

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }


    
    // Return the flight date is modified format "dd/MM/yyyy HH:mm"
    @Override
    public String toString(){
        return sdf.format(this.getDateTime());
    }

}
