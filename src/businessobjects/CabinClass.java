package businessobjects;

/**
 *
 * @author John Wang
 * @studentID 92016019
 *
 */
public enum CabinClass {
    ECONOMY_CLASS, PRESTIGE_CLASS, FIRST_CLASS
}
