/*
 * Main class that starts up the application's Main form 
 */
package uflybookingsystem;

/**
 *
 * @author John Wang
 * @studentID 92016019
 *
 */
public class UFlyBookingSystem {

    public static void main(String[] args) {
        MainForm mainForm = new MainForm();
        mainForm.setVisible(true);
    }    
}
