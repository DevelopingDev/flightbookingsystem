/*
 * Attempt to acquire a connection with the uFly Database
 *
 */
package uflybookingsystem;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author John Wang
 * @studentID 92016019
 *
 */
public class DbConnector {

    //Return uFly database connection
    public static Connection connectToDb() throws SQLException{

        String url = "jdbc:mysql://localhost:3306/";
        String database = "ufly";
        String userName = "root";
        String password = "password";
        return DriverManager.getConnection(url + database, userName, password);
    }
}
