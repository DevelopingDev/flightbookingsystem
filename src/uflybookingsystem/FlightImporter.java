/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uflybookingsystem;

import businessobjects.Flight;
import businessobjects.Plane;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author John Wang
 * @studentID 92016019
 *
 */
public class FlightImporter extends BaseImporter{

    //Regex initialisation
    Pattern pattern;
    Matcher matcher;
    
    ImportResult results = new ImportResult();
    
    public FlightImporter(String fileName) {
        super(fileName);
    }

    @Override
    public void Run() {
        
        
        SimpleDateFormat sdf = new SimpleDateFormat ("dd/MM/yyyy HH:mm:ss");
        
        String srcFile = "Flights.txt";
        String fileData = "";
        
        try (BufferedReader inputFile = new BufferedReader(new FileReader(srcFile))) {
            int ch = 0;

            while ((ch = inputFile.read()) != -1) {
                fileData += (char) ch;
            }

            String[] lines = fileData.replace("\r\n", "\n").replace("\r", "\n").split("\n");
            String firstLine = lines[0];
            
            int lineNum = 0;
            int totalRow = 0;
            int importedRow = 0;
            int failedRow = 0;

            if (FirstLineCheck(firstLine)) {
                
                //TESTING
                System.out.println("Input matches Database!!!");
                
                //remove first element
                ArrayList<String> linesWithoutTitle =  new ArrayList<>();
                Collections.addAll(linesWithoutTitle, lines); 
                linesWithoutTitle.remove(0);
                lines = linesWithoutTitle.toArray(new String[linesWithoutTitle.size()]);
                
                lineNum = 1;
                // loop through lines
                for (String line : lines) {
                    try{
                        
                        if (!line.isEmpty()) {
                            totalRow++;
                            // split current line and assign to colum array
                            String splitter = ",";
                            String[] columns = line.split(splitter);
                            
                            //Flight Number,Departure Airport,Destination Airport,Price,DateTime,Plane,Seats Taken
                            String flightNumber = columns[0]; //
                            String departureAirport = columns[1]; //
                            String destinationAirport = columns[2]; //
                            Double price = Double.parseDouble(columns[3]);
                            Date dateTime = sdf.parse(columns[4]);
                            String strDateTime = columns[4];
                            String plane = columns[5];
                            Integer seatsTaken =Integer.parseInt(columns[6]);
                            
                            // Task 42 
                            if (columns.length == 7) {
                                
                                // Task 41
                                // Check flightNumber departureAirport destinationAirport and plane not null
                                if (!(flightNumber.isEmpty()) && !(departureAirport.isEmpty()) && !(destinationAirport.isEmpty()) && !(plane.isEmpty())) {
                                    
                                    // Task 43
                                    // Check correct flight number format
                                    if (isFlightNumberFormatOk(flightNumber)) {
                                        
                                        
                                        if ("Double".equals(price.getClass().getSimpleName()) && "Date".equals(dateTime.getClass().getSimpleName()) && "Integer".equals(seatsTaken.getClass().getSimpleName())) {
                                            
                                            //Assign plane
                                            Plane selectedPlaneType = null;
                                            
                                            for (Plane planeType : Plane.values()) {
                                                if (planeType.toString().equals(plane)) {
                                                    selectedPlaneType = Plane.valueOf(plane);
                                                    break;
                                                }                                                 
                                            }
                                            
                                            if (selectedPlaneType != null) {
                                                //Check capacity
                                                if (selectedPlaneType.getPassengerCapacity() > seatsTaken) {
                                                    // Insert flight data into database or update if existing

                                                    // if returns nothing then object needed to be added to the list otherwise, info need to be updated not added
                                                    if (DatabaseOperations.getFlightByFlightNumber(flightNumber) == null) {

                                                        //create flightToAdd variable of type location
                                                        //check if this exists in the database by calling addLocation (pass locationToUpdate) from DatabaseOperation class
                                                        // add element to locationList Location array in BookingForm
                                                        Flight flightToAdd = new Flight();
                                                        flightToAdd.setFlightNumber(flightNumber);
                                                        flightToAdd.setDepartureAirport(departureAirport);
                                                        flightToAdd.setDestinationAirport(destinationAirport);
                                                        flightToAdd.setPrice(price);
                                                        flightToAdd.setDateTime(dateTime);
                                                        flightToAdd.setPlane(plane);
                                                        flightToAdd.setSeatsTaken(seatsTaken);
                                                        
                                                        DatabaseOperations.AddFlight(flightToAdd);

                                                        //TESTING
                                                        System.out.println("Flight Added!");
                                                    } else {
                                                        
                                                        Flight flightToUpdate = DatabaseOperations.getFlightByFlightNumber(flightNumber);
                                                        flightToUpdate.setDepartureAirport(departureAirport);
                                                        flightToUpdate.setDestinationAirport(destinationAirport);
                                                        flightToUpdate.setPrice(price);
                                                        flightToUpdate.setDateTime(dateTime);
                                                        flightToUpdate.setPlane(plane);
                                                        flightToUpdate.setSeatsTaken(seatsTaken);
                                                        
                                                        //System.out.println(flightToUpdate.getDateTime());
                                                        // if returns nothing then object needed to be added to the list otherwise, info need to be updated not added
                                                        DatabaseOperations.UpdateFlight(flightToUpdate);
                                                        //TESTING
                                                        System.out.println("Flight Updated");
                                                    }
                                                    
                                                    //Increase number of imported row
                                                    importedRow++;
                                                    results.setImportedRows(importedRow);

                                                } else {
                                                    results = CreateErrorMessage(results, lineNum, "Seats taken can not be bigger than the plane capacity");
                                                    failedRow++;
                                                    //TESTING
                                                    System.out.println("FAILED!!");
                                                }
                                                
                                            } else {
                                                results = CreateErrorMessage(results, lineNum, "Not an existing plane type");
                                                failedRow++;
                                                //TESTING
                                                System.out.println("FAILED!!");
                                            }
                                            
                                            
                                            
                                        } else {
                                            results = CreateErrorMessage(results, lineNum, "Wrong flight number format");
                                            failedRow++;
                                            //TESTING
                                            System.out.println("FAILED!!");
                                        }
                                        
                                        
                                    } else {
                                        results = CreateErrorMessage(results, lineNum, "Wrong flight number format");
                                        failedRow++;
                                        //TESTING
                                        System.out.println("FAILED!!");
                                    }
                                } else {
                                    results = CreateErrorMessage(results, lineNum, "Null value found in either flight number, departure airport, destination airport or plane");
                                    failedRow++;
                                    //TESTING
                                    System.out.println("FAILED!");
                                }
                                
                            } else {
                                // if column number not 2 then add error message to results error message list with approiate message
                                results = CreateErrorMessage(results, lineNum, "Number of columns does not match the location table");
                                failedRow++;
                            }      
                        } 
                        
                    }catch (Exception e){
                        System.err.println("Error when going through importing process: " + e.getMessage());
                        failedRow++;
                    } finally {
                        lineNum++;
                    }
                }
                results.setTotalRows(totalRow);
                results.setFailRows(failedRow);
                System.out.println("Total imported rows: " + results.getImportedRows());
                System.out.println("Total rows: " + results.getTotalRows());
                System.out.println("Total failed rows: " + results.getFailRows());
                //System.out.println("Total row number is: " + totalRow);
            

            } else {
                System.out.println("Input NOT matching Database!!!");
            }

        } catch (FileNotFoundException fnfe) {
            System.err.println("Cannot open the file " + fnfe.getMessage());
        } catch (IOException ioe) {
            System.err.println("Error when processing file " + ioe.getMessage());
        } catch (Exception e) {
            System.err.println("Unknown error has occurred " + e.getMessage());
        }
    }

    private boolean FirstLineCheck(String line) {
        boolean isValid = false;
        //Validation condition 1: Check for column numlineNum = 7
        String splitter = ",";
        String[] columns = line.split(splitter);

        int columnCount = 0;
        for (String column : columns) {
            columnCount++;
            System.out.print("column " + columnCount + ": '" + column.replace(" ", "") + "' ");
        }
        
        System.out.println("Total column count is " + columnCount);
        
        // Check columnCount
        if (columnCount != 7) {
            return false;
        }
        
        //Validation condition 2: Check for column names matching column names in uFly database
        try (Connection connection = DbConnector.connectToDb();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM Flight")) {

            ResultSetMetaData rsmd = resultSet.getMetaData();
            int rsmdColumnCount = rsmd.getColumnCount();
            for (int i = 1; i <= rsmdColumnCount; i++) {
                isValid = columns[(i - 1)].replace(" ", "").equals(rsmd.getColumnName(i));
            }
            return isValid;

        } catch (SQLException sqle) {
            System.out.println(sqle.toString());
        }
        // Always return false by default
        return false;
    }
    
    private ImportResult CreateErrorMessage(ImportResult results, int lineNum, String errorMessage){
        errorMessage = lineNum + " " + errorMessage;
        results.addElementErrorMessages(errorMessage);
        return results;        
    }

    private boolean isFlightNumberFormatOk(String flightNumber) {
                                                     
        Pattern pattern = Pattern.compile("\\w{2}\\d{3}");
        Matcher matcher = pattern.matcher(flightNumber);
        return matcher.matches();
    }
    @Override
    public ImportResult getImportResult(){
        return this.results;
    }
}
