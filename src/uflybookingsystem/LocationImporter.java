/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uflybookingsystem;

import businessobjects.Location;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author John Wang
 * @studentID 92016019
 *
 */
public class LocationImporter extends BaseImporter{

    //Regex initialisation
    Pattern pattern;
    Matcher matcher;
    ImportResult results = new ImportResult();
    
    public LocationImporter(String fileName) {
        super(fileName);
    }

    @Override
    public void Run() {
        
        
        String srcFile = "Locations.txt";
        String fileData = "";
        
        try (BufferedReader inputFile = new BufferedReader(new FileReader(srcFile))) {
            int ch = 0;

            while ((ch = inputFile.read()) != -1) {
                fileData += (char) ch;
            }

            String[] lines = fileData.replace("\r\n", "\n").replace("\r", "\n").split("\n");
            String firstLine = lines[0];
            
            int lineNum = 0;
            int totalRow = 0;
            int importedRow = 0;
            int failedRow = 0;

            if (FirstLineCheck(firstLine)) {
                
                //TESTING
                System.out.println("Input matches Database!!!");
                
                //remove first element
                ArrayList<String> linesWithoutTitle =  new ArrayList<>();
                Collections.addAll(linesWithoutTitle, lines); 
                linesWithoutTitle.remove(0);
                lines = linesWithoutTitle.toArray(new String[linesWithoutTitle.size()]);
                
                lineNum = 1;
                // loop through lines
                for (String line : lines) {
                    try{
                        
                        if (!line.isEmpty()) {
                            totalRow++;
                            // split current line and assign to colum array
                            String splitter = ",";
                            String[] columns = line.split(splitter);
                            String city = columns[0];
                            String airport = columns[1];
                            
                            if (columns.length == 2) {
                                
                                // Check format of city (3 char) using regex
                                if (isAirportFormatOk(airport) && !(airport.isEmpty())) {
                                    
                                    // Check if aiport string is empty
                                    if (!(city.isEmpty())) {
                                        
                                        // update location to database
                                        // create locationToUpdate variable of type location
                                        // check if this exists in the database by calling getLocationByAirportCode (pass airport code) from DatabaseOperation class

                                        // if returns nothing then object needed to be added to the list otherwise, info need to be updated not added
                                        if (DatabaseOperations.getLocationByAirportCode(airport) == null) {
                                            
                                            //create locationToAdd variable of type location
                                            //check if this exists in the database by calling addLocation (pass locationToUpdate) from DatabaseOperation class

                                            // add element to locationList Location array in BookingForm
                                            
                                            Location locationToAdd = new Location();
                                            locationToAdd.setAirportCode(airport);
                                            locationToAdd.setCity(city);
                                            
                                            DatabaseOperations.AddLocation(locationToAdd);
                                            
                                            //TODO: supress the locationList for now
                                            //ArrayList<Location> locationList = BookingForm.locationList;
                                            
                                            //TESTING
                                            System.out.println("Location Added!");
                                        } else {
                                            Location locationToUpdate = DatabaseOperations.getLocationByAirportCode(airport);
                                            locationToUpdate.setCity(city);
                                            // if returns nothing then object needed to be added to the list otherwise, info need to be updated not added
                                            DatabaseOperations.UpdateLocation(locationToUpdate);
                                            //TESTING
                                            System.out.println("Location Updated");
                                        }
                                        //Increase number of imported row
                                        importedRow++;
                                        results.setImportedRows(importedRow);

                                        
                                    } else {
                                        results = CreateErrorMessage(results, lineNum, "City string is empty");
                                        failedRow++;
                                        System.out.println("FAILED!!");
                                    }
                                } else {
                                    results = CreateErrorMessage(results, lineNum, "Airport format is empty or wrong");
                                    failedRow++;
                                    System.out.println("FAILED!");
                                }
                                
                            } else {
                                // if column number not 2 then add error message to results error message list with approiate message
                                results = CreateErrorMessage(results, lineNum, "Number of columns does not match the location table");
                                failedRow++;
                            }      
                        }
                        
                    }catch (Exception e){
                        System.err.println("Error when going through importing process: " + e.getMessage());
                        failedRow++;
                    } finally {
                        lineNum++;
                    }
                }
                results.setTotalRows(totalRow);
                results.setFailRows(failedRow);
                System.out.println("Total imported rows: " + results.getImportedRows());
                System.out.println("Total rows: " + results.getTotalRows());
                System.out.println("Total failed rows: " + results.getFailRows());
                //System.out.println("Total row number is: " + totalRow);

            } else {
                System.out.println("Input NOT matching Database!!!");
            }

        } catch (FileNotFoundException fnfe) {
            System.err.println("Cannot open the file " + fnfe.getMessage());
        } catch (IOException ioe) {
            System.err.println("Error when processing file " + ioe.getMessage());
        } catch (Exception e) {
            System.err.println("Unknown error has occurred " + e.getMessage());
        }
    }

    private boolean FirstLineCheck(String line) {
        boolean isValid = false;
        //Validation condition 1: Check for column numlineNum = 2
        String splitter = ",";
        String[] columns = line.split(splitter);

        int columnCount = 0;
        for (String column : columns) {
            columnCount++;
            System.out.print("column " + columnCount + ": '" + column.replace(" ", "") + "' ");
        }
        
        System.out.println("Total column count is " + columnCount);
        // Check columnCount
        if (columnCount != 2) {
            return false;
        }
        //Validation condition 2: Check for column names matching column names in uFly database
        try (Connection connection = DbConnector.connectToDb();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM Location")) {

            System.out.println("Reading out from Database....");
            ResultSetMetaData rsmd = resultSet.getMetaData();
            int rsmdColumnCount = rsmd.getColumnCount();
            System.out.println("Column count = " + rsmdColumnCount);
            for (int i = 1; i <= rsmdColumnCount; i++) {
                if (columns[(i - 1)].replace(" ", "").equals(rsmd.getColumnName(i))) {
                    System.out.println("Matched: " + rsmd.getColumnName(i));
                    isValid = true;
                } else {
                    isValid = false;
                }
            }
            return isValid;

        } catch (SQLException sqle) {
            System.out.println(sqle.toString());
        }
        // Always return false by default
        return false;
    }
    
    private ImportResult CreateErrorMessage(ImportResult results, int lineNum, String errorMessage){
        errorMessage = lineNum + " " + errorMessage;
        results.addElementErrorMessages(errorMessage);
        return results;        
    }

    private boolean isAirportFormatOk(String city) {
                                                     
        Pattern pattern = Pattern.compile("\\w{3}");
        Matcher matcher = pattern.matcher(city);
        return matcher.matches();
    }
    
    @Override
    public ImportResult getImportResult(){
        return this.results;
    }

}
