/*
 * This class contains all the common operations that involve retreiving data from the database, 
 * saving data to the database or updating existing database data 
 * for all three tables (Location, Flight and Booking)
 */
package uflybookingsystem;

import businessobjects.*;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 *
 * @author John Wang
 * @studentID 92016019
 *
 */
public class DatabaseOperations {
    
    //method that gets all the information from the Location table
    public static ArrayList<Location> GetAllLocations(){
        
        ArrayList<Location> allLocations = new ArrayList<>();
        
        try (Connection connection = DbConnector.connectToDb();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * from location")){
            
            while (resultSet.next()) {
                Location location = new Location();
                location.setCity(resultSet.getString("City"));
                location.setAirportCode(resultSet.getString("AirportCode"));
                allLocations.add(location);                
            }
            
        }
        catch(SQLException sqle){
            System.out.println(sqle.toString());
        }  
        
        return allLocations;
    }
    
    // this method returns all the data from the Flight table in the uFly database
    public static ArrayList<Flight> GetAllFlights(){
        
        ArrayList<Flight> allFlights = new ArrayList<>();
        
        try (Connection connection = DbConnector.connectToDb();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * from flight")){
            
            while (resultSet.next()) {
                Flight flight = new Flight();
                flight.setFlightNumber(resultSet.getString("FlightNumber"));
                flight.setDepartureAirport(resultSet.getString("DepartureAirport"));
                flight.setDestinationAirport(resultSet.getString("DestionationAirport"));
                flight.setPrice(resultSet.getDouble("Price"));
                flight.setDateTime(resultSet.getTimestamp("DateTime"));
                // Set correct plane enum based on String value from resultset
                flight.setPlane(resultSet.getString("Plane"));
                flight.setSeatsTaken(resultSet.getInt("SeatsTaken"));
                
                allFlights.add(flight);                 
            }
            
        }
        catch(SQLException sqle){
            System.out.println(sqle.toString());
        }  
        
        return allFlights;
    }
    
    //this method obtains all the information from the Flight table based on the departure and destination airports as well as travel date
    public static ArrayList<Flight> GetAllFlightsForLocation(String departure, String destination, java.util.Date travelDate){
        
        ArrayList<Flight> allFlightsForLocation = new ArrayList<>();
 
        
        try (Connection connection = DbConnector.connectToDb();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * from flight WHERE DepartureAirport = " + departure + " AND DestinationAirport = " + destination)){
            
            while (resultSet.next()) {
                Flight flight = new Flight();
                flight.setFlightNumber(resultSet.getString("FlightNumber"));
                flight.setDepartureAirport(resultSet.getString("DepartureAirport"));
                flight.setDestinationAirport(resultSet.getString("DestionationAirport"));
                flight.setPrice(resultSet.getDouble("Price"));
                flight.setDateTime(resultSet.getTimestamp("DateTime"));
                // Set correct plane enum based on String value from resultset
                flight.setPlane(resultSet.getString("Plane"));
                flight.setSeatsTaken(resultSet.getInt("SeatsTaken"));
                
                allFlightsForLocation.add(flight);                 
            }
            
        }
        catch(SQLException sqle){
            System.out.println(sqle.toString());
        }  
        
        
        return allFlightsForLocation;
    }
    
    //this method adds booking passed as a parameter to the Booking table in the uFly database
    //note that Booking number is set as an incrementing field, so it doesn't need to be set
    public static void AddBooking(Booking booking){
        try (Connection connection = DbConnector.connectToDb();
             Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
             ResultSet resultSet = statement.executeQuery("SELECT * from Booking")){
            
            resultSet.moveToInsertRow();
            resultSet.updateString("FlightNumber", booking.getFlightNumber());
            resultSet.updateDouble("Price", booking.getPrice());
            resultSet.updateString("CabinClass", booking.getCabinClass());
            resultSet.updateInt("Quantity", booking.getQuantity());
            resultSet.insertRow();
        }
        catch(SQLException sqle){
            System.out.println(sqle.toString());
        } 
        
    }
    
    //this method obtains the flight based on the flightNumber parameter
    public static Flight getFlightByFlightNumber(String flightNumber){
        
        Flight flightByFLightNumber = new Flight();
        
        
        try (Connection connection = DbConnector.connectToDb();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * from Flight "
                    + "WHERE FlightNumber = '" + flightNumber + "'")){
            
            while (resultSet.next()) {
                Flight flightByFlightNumber = new Flight();
                flightByFlightNumber.setFlightNumber(resultSet.getString("FlightNumber"));
                flightByFlightNumber.setDepartureAirport(resultSet.getString("DepartureAirport"));
                flightByFlightNumber.setDestinationAirport(resultSet.getString("DestinationAirport"));
                flightByFlightNumber.setPrice(resultSet.getDouble("Price"));
                flightByFlightNumber.setDateTime(resultSet.getTimestamp("DateTime"));
                flightByFlightNumber.setPlane(resultSet.getString("Plane"));
                flightByFlightNumber.setSeatsTaken(resultSet.getInt("SeatsTaken"));
            	return flightByFlightNumber;
            }
        }
        catch(SQLException sqle){
            System.out.println(sqle.toString());
        }  
        return null;
    }
    
     //this method obtains the flight based on the flightNumber parameter
   public static Location getLocationByAirportCode(String airportCode){
	try (Connection connection = DbConnector.connectToDb();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * from Location "
                        + "WHERE AirportCode = '" + airportCode + "'")) {

            while (resultSet.next()) {
                Location location = new Location();
                location.setCity(resultSet.getString("City"));
                location.setAirportCode(resultSet.getString("AirportCode"));
                return location;
            }
            
        }
        catch(SQLException sqle){
            System.out.println(sqle.toString());
        }  
        
        return null;
    }
    
    //this method adds location passed as a parameter to the Location table in the uFly database
    public static void AddLocation(Location location){
	try (Connection connection = DbConnector.connectToDb();
             Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
             ResultSet resultSet = statement.executeQuery("SELECT City, AirportCode from Location")){
            
            resultSet.moveToInsertRow();
            resultSet.updateString("City", location.getCity());
            resultSet.updateString("AirportCode", location.getAirportCode());
            resultSet.insertRow();
        }
        catch(SQLException sqle){
            System.out.println(sqle.toString());
        } 
    }
     
    //this method adds a flight passed as a parameter to the Flight table in the uFly database
    public static void AddFlight(Flight flight){
        try (Connection connection = DbConnector.connectToDb();
             Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
             ResultSet resultSet = statement.executeQuery("SELECT * from Flight")){
            
            resultSet.moveToInsertRow();
            resultSet.updateString("FlightNumber", flight.getFlightNumber());
            resultSet.updateString("DepartureAirport", flight.getDepartureAirport());
            resultSet.updateString("DestinationAirport", flight.getDestinationAirport());
            resultSet.updateDouble("Price", flight.getPrice());
            
            //System.out.println(new java.sql.Date(flight.getDateTime().getTime()));
            java.sql.Timestamp timeStampDate = new Timestamp(flight.getDateTime().getTime());
            resultSet.updateTimestamp("DateTime", timeStampDate);
            resultSet.updateString("Plane", flight.getPlane());
            resultSet.updateInt("SeatsTaken", flight.getSeatsTaken());
            resultSet.insertRow();
        }
        catch(SQLException sqle){
            System.out.println(sqle.toString());
        } 
    }

    
    //this method updates the location to the one passed to it as a parameter where the airport codes are matching
    public static void UpdateLocation(Location location){
        try (Connection connection = DbConnector.connectToDb();
            Statement statement = connection.createStatement();){
            statement.executeUpdate("Update Location SET City = '" + location.getCity() + "' WHERE AirportCode = '" + location.getAirportCode() + "'");
        }
        catch(SQLException sqle){
            System.out.println(sqle.toString());
        }  
    }
    
    //this method updates the flight to the one passed to it as a parameter where the flight numbers are matching
    public static void UpdateFlight(Flight flight){
        SimpleDateFormat sdf = new SimpleDateFormat ("dd/MM/yyyy HH:mm:ss");
        try (Connection connection = DbConnector.connectToDb();
            Statement statement = connection.createStatement();){
            java.sql.Timestamp timeStampDate = new Timestamp(flight.getDateTime().getTime());
            String updateStatement = "Update Flight SET DepartureAirport = '" + flight.getDepartureAirport() 
                    + "', DestinationAirport = '" + flight.getDestinationAirport() 
                    + "', Price = " + flight.getPrice() 
                    //??
                    + ", DateTime = '" + timeStampDate
                    + "', Plane = '" + flight.getPlane()
                    + "', SeatsTaken = " + flight.getSeatsTaken()
                    + " WHERE FlightNumber = '" + flight.getFlightNumber() + "'";
            System.out.println(updateStatement);
            statement.executeUpdate(updateStatement);
        }
        catch(SQLException sqle){
            System.out.println(sqle.toString());
        }  
    }
}
