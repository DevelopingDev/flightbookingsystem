/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uflybookingsystem;


import java.util.ArrayList;


/**
 *
 * @author John Wang
 * @studentID 92016019
 *
 */
class ImportResult {
    
    private int totalRows;

    private int importedRows;
    
    private int failRows;
    
    private ArrayList<String> errorMessages;

    public ArrayList<String> getErrorMessages() {
        return errorMessages;
    }
    
    public void addElementErrorMessages(String message){
        this.errorMessages.add(message);
    }

    public int getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public int getImportedRows() {
        return importedRows;
    }

    public void setImportedRows(int importedRows) {
        this.importedRows = importedRows;
    }

    public int getFailRows() {
        return failRows;
    }

    public void setFailRows(int failRows) {
        this.failRows = failRows;
    }

    public ImportResult() {
        totalRows = 0;
        importedRows = 0;
        failRows = 0;
        this.errorMessages = new ArrayList<>();
        errorMessages.clear();
    }

}
