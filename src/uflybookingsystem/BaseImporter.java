/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uflybookingsystem;

/**
 *
 * @author John Wang
 * @studentID 92016019
 *
 */
public abstract class BaseImporter implements Runnable {

    private ImportResult ImportResult;
    
    private String fileName;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public ImportResult getImportResult() {
        return ImportResult;
    }

    public void setImportResult(ImportResult ImportResult) {
        this.ImportResult = ImportResult;
    }
    
    public BaseImporter(String fileName) {
        this.fileName = fileName;
    }
    
}
