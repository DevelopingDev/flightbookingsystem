# **uFly Flight Booking System** #

This is a private project for Computer Power Plus Java2 (JA2) module.

## Purpose

A flight booking computer system that will allow bookings to be made and data on the destination and the flights that the company is working with to be available through the MySQL database.

## Usage

TODO: Write usage instructions

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request

## Credits

Author: John Wang

## License

Private view/develope/usage for John Wang and marking purposes only